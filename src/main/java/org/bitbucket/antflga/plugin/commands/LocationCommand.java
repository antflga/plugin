package org.bitbucket.antflga.plugin.commands;

import org.bitbucket.antflga.plugin.main.Plugin;
import org.bitbucket.antflga.plugin.util.WorldUtils;
import org.bitbucket.antflga.plugin.util.YmlUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author alice
 * @since 9/20/2015
 */
@SuppressWarnings("unused")
public class LocationCommand extends CommandBase {

    int i = 0;

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        int length = args.length;

        List<Integer> playerCoordinates = new ArrayList<>();
        playerCoordinates.add(player.getLocation().getBlockX());
        playerCoordinates.add(player.getLocation().getBlockY());
        playerCoordinates.add(player.getLocation().getBlockZ());

        setExecuted(false);

        Arrays.asList(args).forEach(arg -> {
            if (!getExecuted()) {
                if (length == 1) {
                    if (arg.equals("list")) {
                        Set nodeSet = Plugin.getLocationConfig().getConfigurationSection("plugin.locations").getKeys(true);
                        List<String> nodes = new ArrayList<>(nodeSet);
                        nodes.forEach(node -> {
                            player.sendMessage("Name: " + node + "\n" +
                                    "Coordinates: " + YmlUtils.readNode("plugin.locations", nodes.get(i)));
                            i++;
                        });

                        setExecuted(true);
                    }
                    if (arg.equals("wipe")) {
                        player.sendMessage("dedening ur locations m8");
                        WorldUtils.wipeLocations();
                        setExecuted(true);
                    }
                }
                if (length == 2) {
                    Arrays.asList(args).forEach(player::sendMessage);
                    if (args[0].equals("create")) {
                        WorldUtils.writeLocation(player, args[1], playerCoordinates);
                        setExecuted(true);
                    } else if (args[0].equals("clear")) {
                        WorldUtils.removeLocation(player, args[1]);
                        setExecuted(true);
                    }
                }
            }
        });

        return getExecuted();
    }
}