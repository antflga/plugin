package org.bitbucket.antflga.plugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public class Mute extends CommandBase {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        return false;
    }
}
