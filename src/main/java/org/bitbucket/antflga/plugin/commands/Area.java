package org.bitbucket.antflga.plugin.commands;

import org.bitbucket.antflga.plugin.main.Plugin;
import org.bitbucket.antflga.plugin.util.WorldUtils;
import org.bitbucket.antflga.plugin.util.YmlUtils;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


/**
 * @author alice
 * @since 9/20/2015
 */
@SuppressWarnings("unused")
public class Area extends CommandBase {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        Player player = (Player) commandSender;
        int length = args.length;

        Arrays.asList(args).forEach(arg -> {
            if (length == 1) {
                if (arg.equals("list")) {
                    Set nodeSet = Plugin.getLocationConfig().getConfigurationSection("plugin.areas").getKeys(true);
                    List<String> nodes = new ArrayList<>(nodeSet);
                    if (nodes.size() == 0) {
                        setExecuted(true);
                    }
                    player.sendMessage("Name: " + nodes.get(0) + "\n" +
                            "Locations: " + YmlUtils.readNode("plugin.areas", nodes.get(1)) + "\n" +
                            "Coordinates: " + YmlUtils.readNode("plugin.areas", nodes.get(3)) + ", " +
                            YmlUtils.readNode("plugin.areas", nodes.get(4)));

                    setExecuted(true);
                }
                if (arg.equals("wipe")) {
                    WorldUtils.wipeAreas();
                    setExecuted(true);
                }
            }
            if (length == 2) {
                if (arg.equals("fill")) {
                    WorldUtils.placeAtCoordinates(player, args[1], Material.GOLD_ORE, true);
                    setExecuted(true);
                }
                if (arg.equals("clear")) {
                    WorldUtils.placeAtCoordinates(player, args[1], Material.AIR, false);
                    setExecuted(true);
                }
                if (arg.equals("delete")) {
                    WorldUtils.removeArea(player, args[1]);
                    setExecuted(true);
                }
            }
            if (length == 4) {
                if (arg.equals("create")) {
                    WorldUtils.writeArea(player, args[3], args[1], args[2]);
                    setExecuted(true);
                }
            }
        });

        return getExecuted();
    }
}