package org.bitbucket.antflga.plugin.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * @author alice
 * @since 9/30/2015
 */
@SuppressWarnings("unused")
public abstract class CommandBase implements CommandExecutor {

    private boolean execution;

    public boolean getExecuted() {
        return execution;
    }

    public void setExecuted(boolean executed) {
        this.execution = executed;
    }

    public abstract boolean onCommand(CommandSender commandSender, Command command, String s, String[] args);
}
