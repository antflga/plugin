package org.bitbucket.antflga.plugin.pvp.heroes;

import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.util.ArmorUtils;
import org.bukkit.entity.Player;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public class Scout implements GenericHero {

    @Override
    public void createHero(Player player) {
        int[] rgb = new int[3];
        rgb[0] = 255;
        rgb[1] = 0;
        rgb[2] = 0;
        ArmorUtils.setAllLeatherArmor(player, rgb);
    }
}
