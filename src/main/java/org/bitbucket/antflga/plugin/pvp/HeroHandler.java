package org.bitbucket.antflga.plugin.pvp;

import org.bitbucket.antflga.plugin.commands.CommandBase;
import org.bitbucket.antflga.plugin.pvp.heroes.*;
import org.bitbucket.antflga.plugin.pvp.heroes.donator.Blaze;
import org.bitbucket.antflga.plugin.pvp.heroes.donator.Mage;
import org.bitbucket.antflga.plugin.pvp.heroes.donator.SnowGolem;
import org.bitbucket.antflga.plugin.pvp.heroes.donator.ZombiePigman;
import org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus.ArchMage;
import org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus.Ninja;
import org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus.Spy;
import org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus.Squid;
import org.bitbucket.antflga.plugin.util.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public class HeroHandler extends CommandBase {

    public static GenericHero[] heroes = {
            new Assassin(),
            new Alchemist(),
            new Archer(),
            new Knight(),
            new Scout(),
            new Tank(),
            new Warrior(),
            new Blaze(),
            new Mage(),
            new SnowGolem(),
            new ZombiePigman(),
            new ArchMage(),
            new Ninja(),
            new Spy(),
            new Squid()
    };

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        int length = args.length;

        Arrays.asList(args).forEach(arg -> {
            if (length == 1) {
                if (arg.equals("list")) {
                    Arrays.asList(heroes).forEach(hero -> player.sendMessage(hero.getClass().getSimpleName().toLowerCase()));
                    setExecuted(true);
                }
                if (arg.equals("clear")) {
                    PlayerUtils.getInstance().removeEntry(player);
                    player.getInventory().clear();
                    PlayerUtils.getInstance().removePlayerModifiers(player);
                    setExecuted(true);
                }
                Arrays.asList(heroes).forEach(hero -> {
                    if (arg.equals(hero.getClass().getSimpleName().toLowerCase())) {
                        PlayerUtils.getInstance().removePlayerModifiers(player);
                        hero.createHero(player);
                        PlayerUtils.getInstance().setTag(player, arg);
                        setExecuted(true);
                    }
                });
            }
            if (length == 2) {
                Arrays.asList(heroes).forEach(hero -> {
                    if (arg.equals(hero.getClass().getSimpleName().toLowerCase())) {
                        PlayerUtils.getInstance().removePlayerModifiers(player);
                        Player target = Bukkit.getServer().getPlayer(args[1]);
                        hero.createHero(target);
                        PlayerUtils.getInstance().setTag(target, arg);
                        setExecuted(true);
                    }
                });
            }
            if (!getExecuted()) {
                player.sendMessage("You fucked something up");
            }
        });

        return getExecuted();
    }
}