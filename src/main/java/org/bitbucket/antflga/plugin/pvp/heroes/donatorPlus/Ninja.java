package org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus;

import net.md_5.bungee.api.ChatColor;
import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.util.ArmorUtils;
import org.bitbucket.antflga.plugin.util.InventoryUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public class Ninja implements GenericHero {

    @Override
    public void createHero(Player player) {
        int[] rgb = new int[3];
        rgb[0] = 0;
        rgb[1] = 0;
        rgb[2] = 0;
        ArmorUtils.setAllLeatherArmor(player, rgb);
        ItemStack wand = new ItemStack(Material.STICK);
        ItemMeta wandMeta = wand.getItemMeta();
        wandMeta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + "Bamboo rod of relocation");
        List<String> lore = new ArrayList<>();
        lore.add("Coveted by ninjas in the east");
        lore.add("this rod is imbued with divine");
        lore.add("power too complex for all but");
        lore.add("the ninja to harness.");
        wandMeta.setLore(lore);
        wand.setItemMeta(wandMeta);
        wand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
        InventoryUtils.addToInventory(player, 0, wand);
        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
    }
}
