package org.bitbucket.antflga.plugin.pvp.heroes;

import net.md_5.bungee.api.ChatColor;
import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.util.ArmorType;
import org.bitbucket.antflga.plugin.util.ArmorUtils;
import org.bitbucket.antflga.plugin.util.InventoryUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public class Alchemist implements GenericHero {

    @Override
    public void createHero(Player player) {
        ArmorUtils.setArmor(player, ArmorType.GOLD);
        ItemStack wand = new ItemStack(Material.BLAZE_ROD);
        ItemMeta wandMeta = wand.getItemMeta();
        wandMeta.setDisplayName(ChatColor.RESET + "" + ChatColor.GOLD + "Wand Of Alchemy");
        List<String> lore = new ArrayList<>();
        lore.add("Forged by the ogres in the fires");
        lore.add("of the mountains in which they");
        lore.add("reside, this wand is coveted for its");
        lore.add("magnificent power.");
        wandMeta.setLore(lore);
        wand.setItemMeta(wandMeta);
        wand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
        InventoryUtils.addToInventory(player, 0, wand);
        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 2));
    }
}
