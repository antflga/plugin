package org.bitbucket.antflga.plugin.pvp;

import org.bukkit.entity.Player;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public interface GenericHero {

    void createHero(Player player);
}
