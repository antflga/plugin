package org.bitbucket.antflga.plugin.pvp;

import net.md_5.bungee.api.ChatColor;
import org.bitbucket.antflga.plugin.main.Plugin;
import org.bitbucket.antflga.plugin.util.Cooldown;
import org.bitbucket.antflga.plugin.util.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

import java.util.Set;

/**
 * @author alice
 * @since 9/20/2015
 */
@SuppressWarnings("unused")
public class PVPListener implements Listener {

    Plugin plugin;
    Location loc;
    private int archmageTask, ninjaTask, alchTask, splashTask, alchLoops;

    public PVPListener(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
    }


    @EventHandler
    public void playerPickupItemEvent(PlayerPickupItemEvent event) {
        if (PlayerUtils.getInstance().getTag(event.getPlayer()) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void playerDeathEvent(PlayerDeathEvent event) {
        PlayerUtils.getInstance().removeEntry(event.getEntity().getPlayer());
        event.getDrops().clear();
    }

    @EventHandler
    public void PotionSplashEvent(PotionSplashEvent event) {
        for (PotionEffect pe : event.getPotion().getEffects()) {
            event.getAffectedEntities().stream().filter(le -> pe.getType().equals(PotionEffectType.HARM) && le.equals(event.getEntity().getShooter())).forEach(le -> event.setCancelled(true));
        }

        if (event.getEntity().getName().equals("alchemistPotion")) {
            Location location = event.getEntity().getLocation();
            int xm3 = location.getBlockX() - 3;
            int xf3 = location.getBlockX() + 3;
            int zm3 = location.getBlockZ() - 3;
            int zf3 = location.getBlockZ() + 3;
            int ym3 = location.getBlockY();
            int yf3 = location.getBlockY() + 6;

            splashTask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
                alchLoops--;
                if (alchLoops == 0) {
                    Bukkit.getScheduler().cancelTask(splashTask);
                    Bukkit.getScheduler().cancelTask(alchTask);
                }
                if (alchLoops > 0) {
                    for (int x = xm3; x - 1 < xf3; x++) {
                        for (int y = ym3; y - 1 < yf3; y++) {
                            for (int z = zm3; z - 1 < zf3; z++) {
                                loc = new Location(event.getEntity().getWorld(), x, y, z);
                                Bukkit.getOnlinePlayers().forEach(player -> {
                                    player.playEffect(loc, Effect.WITCH_MAGIC, null);
                                    player.playEffect(loc, Effect.LARGE_SMOKE, null);
                                    player.playEffect(loc, Effect.EXTINGUISH, null);
                                    if (player.getLocation().distance(loc) < 3 && !player.equals(event.getEntity().getShooter())) {
                                        player.damage(1, (Player) event.getEntity().getShooter());
                                    }
                                });
                            }
                        }
                    }
                }
            }, 10, 10);
        }
    }

    public Location getBlockBelowLoc(Location loc, Player player) {
        Location locBelow = loc.subtract(0, 1, 0);
        if (locBelow.getBlock().getType() == Material.AIR) {
            locBelow = getBlockBelowLoc(locBelow, player);
        }
        return locBelow;
    }

    @EventHandler
    public void foodLevelChangeEvent(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void playerInteractEvent(PlayerInteractEvent event) {

        if (PlayerUtils.getInstance().getTag(event.getPlayer()).equals("archmage")) {
            if (event.getPlayer().getInventory().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RESET + "" + ChatColor.DARK_PURPLE + "Divine wand of destruction")) {
                if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    event.setCancelled(true);
                    if (Cooldown.getCooldown(event.getPlayer()) == 0) {
                        ItemStack star = new ItemStack(Material.FIREWORK_CHARGE);
                        Entity spell = event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getEyeLocation(), star);
                        spell.setVelocity(event.getPlayer().getLocation().getDirection());
                        Vector velocity = spell.getVelocity();
                        Bukkit.getOnlinePlayers().forEach(player -> player.playEffect(event.getPlayer().getLocation(), Effect.GHAST_SHOOT, null));
                        Cooldown.setCooldown(event.getPlayer(), 2);
                        archmageTask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
                            if (spell.getTicksLived() > 500) spell.remove();
                            if (spell.isOnGround()) spell.remove();
                            Bukkit.getOnlinePlayers().forEach(player -> {
                                if (spell.getVelocity() != velocity) {
                                    spell.setVelocity(velocity);
                                }
                                if (!spell.isDead())
                                    player.playEffect(spell.getLocation(), Effect.FIREWORKS_SPARK, null);
                                if (spell.getLocation().distance(player.getLocation()) < 2 && player != event.getPlayer()) {
                                    spell.remove();
                                    player.playEffect(player.getLocation(), Effect.EXPLOSION_LARGE, null);
                                    player.damage(12, event.getPlayer());

                                }
                            });
                        }, 3, 3);
                    } else {
                        Cooldown.cooldownMessage(event.getPlayer());
                    }
                }
            }
        }

        if (PlayerUtils.getInstance().getTag(event.getPlayer()).equals("ninja")) {
            if (event.getPlayer().getInventory().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RESET + "" + ChatColor.GREEN + "Bamboo rod of relocation")) {
                if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (Cooldown.getCooldown(event.getPlayer()) == 0) {
                        Block block = event.getPlayer().getTargetBlock((Set<Material>) null, 10);
                        Location loc = block.getLocation();
                        loc = getBlockBelowLoc(loc, event.getPlayer()).add(0, 2, 0);
                        loc.setPitch(event.getPlayer().getLocation().getPitch());
                        loc.setYaw(event.getPlayer().getLocation().getYaw());
                        event.getPlayer().teleport(loc);
                        Cooldown.setCooldown(event.getPlayer(), 5);
                        event.getPlayer().playEffect(event.getPlayer().getLocation(), Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
                        event.getPlayer().playEffect(event.getPlayer().getLocation(), Effect.GHAST_SHOOT, null);
                        event.getPlayer().sendMessage(ChatColor.GREEN + "" + ChatColor.ITALIC + "You feel ancestral power course through your veins.");
                    } else {
                        Cooldown.cooldownMessage(event.getPlayer());
                    }
                    event.setCancelled(true);
                }
                if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    ItemStack star = new ItemStack(Material.NETHER_STAR);
                    Entity ninjaStar = event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getEyeLocation(), star);
                    ninjaStar.setVelocity(event.getPlayer().getLocation().getDirection());
                    ninjaTask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
                        if (ninjaStar.getTicksLived() > 100) ninjaStar.remove();
                        if (ninjaStar.isOnGround()) ninjaStar.remove();
                        Bukkit.getOnlinePlayers().forEach(player -> {
                            if (ninjaStar.getLocation().distance(player.getLocation()) < 2 && player != event.getPlayer()) {
                                ninjaStar.remove();
                                player.playEffect(player.getLocation(), Effect.FIREWORKS_SPARK, null);
                                player.damage(2, event.getPlayer());
                            }
                        });
                    }, 3, 3);
                    event.setCancelled(true);
                }
            }
        }

        if (PlayerUtils.getInstance().getTag(event.getPlayer()).equals("alchemist")) {
            if (event.getPlayer().getInventory().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RESET + "" + ChatColor.GOLD + "Wand Of Alchemy")) {
                if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    event.setCancelled(true);
                    if (Cooldown.getCooldown(event.getPlayer()) == 0) {
                        alchLoops = 6;
                        Cooldown.setCooldown(event.getPlayer(), 5);
                        alchTask = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                            Potion potion = new Potion(PotionType.INSTANT_DAMAGE);
                            potion.setSplash(true);
                            ItemStack pot = new ItemStack(Material.POTION);
                            potion.apply(pot);
                            ThrownPotion entity = event.getPlayer().launchProjectile(ThrownPotion.class);
                            entity.setCustomName("alchemistPotion");
                            entity.setItem(pot);
                            entity.setShooter(event.getPlayer());
                        }, 5);
                    } else {
                        Cooldown.cooldownMessage(event.getPlayer());
                    }
                }
            }
        }
    }
}