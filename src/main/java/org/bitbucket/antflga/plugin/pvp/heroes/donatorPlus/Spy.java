package org.bitbucket.antflga.plugin.pvp.heroes.donatorPlus;

import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.util.ArmorUtils;
import org.bukkit.entity.Player;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public class Spy implements GenericHero {

    @Override
    public void createHero(Player player) {
        int[] rgb = new int[3];
        rgb[0] = 0;
        rgb[1] = 0;
        rgb[2] = 0;
        ArmorUtils.setAllLeatherArmor(player, rgb);
    }
}
