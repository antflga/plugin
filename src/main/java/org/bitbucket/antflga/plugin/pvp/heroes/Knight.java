package org.bitbucket.antflga.plugin.pvp.heroes;

import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.util.ArmorType;
import org.bitbucket.antflga.plugin.util.ArmorUtils;
import org.bukkit.entity.Player;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public class Knight implements GenericHero {

    @Override
    public void createHero(Player player) {
        ArmorUtils.setArmor(player, ArmorType.CHAINMAIL);
    }
}
