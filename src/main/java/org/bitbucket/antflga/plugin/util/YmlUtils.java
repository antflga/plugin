package org.bitbucket.antflga.plugin.util;

import org.bitbucket.antflga.plugin.main.Plugin;

import java.util.List;

/**
 * @author alice
 * @since 9/24/2015
 */
@SuppressWarnings("unused")
public final class YmlUtils {

    public static Object getNode(String section, String node) {
        return Plugin.getLocationConfig().getConfigurationSection(section).get(node);
    }

    public static List<Integer> getIntegerList(String section, String node) {
        return Plugin.getLocationConfig().getIntegerList(section + "." + node);
    }

    public static void setNode(String section, String node, Object data) {
        Plugin.getLocationConfig().getConfigurationSection(section).set(node, data);
        Plugin.saveYml(Plugin.getLocationConfig(), Plugin.getLocationYml());
    }

    public static String readNode(String section, String node) {
        return Plugin.getLocationConfig().getConfigurationSection(section).get(node).toString();
    }

    public static void deleteNode(String node) {
        Plugin.getLocationConfig().set(node, null);
        Plugin.saveYml(Plugin.getLocationConfig(), Plugin.getLocationYml());
    }
}
