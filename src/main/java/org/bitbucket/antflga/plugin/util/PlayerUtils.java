package org.bitbucket.antflga.plugin.util;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.HashMap;

/**
 * @author alice
 * @since 9/20/2015
 */
@SuppressWarnings("unused")
public final class PlayerUtils {

    private static PlayerUtils instance = null;
    private HashMap<String, String> tags = new HashMap<>();

    protected PlayerUtils() {

    }

    public static PlayerUtils getInstance() {
        if (instance == null) {
            instance = new PlayerUtils();
        }
        return instance;
    }

    public void setTag(Player player, String tag) {
        tags.put(player.getName(), tag);
    }

    public void removeEntry(Player player) {
        tags.remove(player.getName());
    }

    public String getTag(Player player) {
        return tags.get(player.getName());
    }

    public void removePlayerModifiers(Player player) {
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
    }
}
