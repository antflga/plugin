package org.bitbucket.antflga.plugin.util;

import org.bitbucket.antflga.plugin.main.Plugin;
import org.bitbucket.antflga.plugin.pvp.GenericHero;
import org.bitbucket.antflga.plugin.pvp.HeroHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author alice
 * @since 10/6/2015
 */
@SuppressWarnings("unused")
public class HeroAssigner extends BukkitRunnable {

    Plugin plugin;
    HashMap<GenericHero, Location> locations = new HashMap<>();

    public HeroAssigner(Plugin plugin) {
        this.plugin = plugin;
    }

    int i = 0;

    @Override
    public void run() {
        Arrays.asList(HeroHandler.heroes).forEach(hero -> Bukkit.getOnlinePlayers().forEach(player -> {
            List<Integer> intList = YmlUtils.getIntegerList("plugin.locations", hero.getClass().getSimpleName().toLowerCase());
            Location loc = new Location(player.getWorld(), intList.get(0), intList.get(1), intList.get(2));
            locations.put(hero, loc);

            if (player.getLocation().distance(locations.get(hero)) < 1) {
                PlayerUtils.getInstance().removePlayerModifiers(player);
                hero.createHero(player);
                PlayerUtils.getInstance().setTag(player, hero.getClass().getSimpleName().toLowerCase());
            }
        }));
    }
}
