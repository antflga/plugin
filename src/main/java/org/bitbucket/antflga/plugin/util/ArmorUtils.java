package org.bitbucket.antflga.plugin.util;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Arrays;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public final class ArmorUtils {

    private static void setArmor(Player player, ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots) {
        player.getInventory().setHelmet(helmet);
        player.getInventory().setChestplate(chestplate);
        player.getInventory().setLeggings(leggings);
        player.getInventory().setBoots(boots);
    }

    public static void setArmor(Player player, ArmorType armorType) {
        switch (armorType) {
            case CHAINMAIL:
                setArmor(player, new ItemStack(Material.CHAINMAIL_HELMET), new ItemStack(Material.CHAINMAIL_CHESTPLATE), new ItemStack(Material.CHAINMAIL_LEGGINGS), new ItemStack(Material.CHAINMAIL_BOOTS));
                break;

            case GOLD:
                setArmor(player, new ItemStack(Material.GOLD_HELMET), new ItemStack(Material.GOLD_CHESTPLATE), new ItemStack(Material.GOLD_LEGGINGS),
                        new ItemStack(Material.GOLD_BOOTS));
                break;

            case IRON:
                setArmor(player, new ItemStack(Material.IRON_HELMET), new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_LEGGINGS),
                        new ItemStack(Material.IRON_BOOTS));
                break;

            case DIAMOND:
                setArmor(player, new ItemStack(Material.DIAMOND_HELMET), new ItemStack(Material.DIAMOND_CHESTPLATE), new ItemStack(Material.DIAMOND_LEGGINGS),
                        new ItemStack(Material.DIAMOND_BOOTS));
                break;
        }
    }

    public static void setLeatherArmor(Player player, int[] helmetRGB, int[] chestplateRGB, int[] leggingsRGB,
                                       int[] bootsRGB) {
        ItemStack[] armor = {
                new ItemStack(Material.LEATHER_HELMET), new ItemStack(Material.LEATHER_CHESTPLATE),
                new ItemStack(Material.LEATHER_LEGGINGS), new ItemStack(Material.LEATHER_BOOTS)
        };

        LeatherArmorMeta[] meta = {
                (LeatherArmorMeta) armor[0].getItemMeta(), (LeatherArmorMeta) armor[1].getItemMeta(),
                (LeatherArmorMeta) armor[2].getItemMeta(), (LeatherArmorMeta) armor[3].getItemMeta()
        };

        meta[0].setColor(Color.fromRGB(helmetRGB[0], helmetRGB[1], helmetRGB[2]));
        meta[1].setColor(Color.fromRGB(chestplateRGB[0], chestplateRGB[1], chestplateRGB[2]));
        meta[2].setColor(Color.fromRGB(leggingsRGB[0], leggingsRGB[1], leggingsRGB[2]));
        meta[3].setColor(Color.fromRGB(bootsRGB[0], bootsRGB[1], bootsRGB[2]));

        for (int i = 0; i < armor.length; i++) {
            armor[i].setItemMeta(meta[i]);
        }

        setArmor(player, armor[0], armor[1], armor[2], armor[3]);
    }

    public static void setAllLeatherArmor(Player player, int[] RGB) {
        ItemStack[] armor = {
                new ItemStack(Material.LEATHER_HELMET), new ItemStack(Material.LEATHER_CHESTPLATE),
                new ItemStack(Material.LEATHER_LEGGINGS), new ItemStack(Material.LEATHER_BOOTS)
        };

        LeatherArmorMeta[] meta = {
                (LeatherArmorMeta) armor[0].getItemMeta(), (LeatherArmorMeta) armor[1].getItemMeta(),
                (LeatherArmorMeta) armor[2].getItemMeta(), (LeatherArmorMeta) armor[3].getItemMeta()
        };

        Arrays.asList(meta).forEach(leatherArmorMeta -> {
            leatherArmorMeta.setColor(Color.fromRGB(RGB[0], RGB[1], RGB[2]));
            Arrays.asList(armor).forEach(itemStack -> itemStack.setItemMeta(leatherArmorMeta));
        });

        setArmor(player, armor[0], armor[1], armor[2], armor[3]);
    }
}