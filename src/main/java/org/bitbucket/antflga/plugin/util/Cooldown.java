package org.bitbucket.antflga.plugin.util;

import org.bitbucket.antflga.plugin.main.Plugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

/**
 * @author alice
 * @since 9/24/2015
 */
@SuppressWarnings("unused")
public class Cooldown extends BukkitRunnable {

    private static HashMap<String, Integer> cooldowns = new HashMap<>();
    private Plugin plugin;

    public Cooldown(Plugin plugin) {
        this.plugin = plugin;
    }

    public static int getCooldown(Player player) {
        return cooldowns.get(player.getName());
    }

    public static void setCooldown(Player player, int cooldownDuration) {
        cooldowns.put(player.getName(), cooldownDuration);
    }

    public static void cooldownMessage(Player player) {
        if (getCooldown(player) == 1) {
            player.sendMessage("On cooldown for 1 second!");
        } else {
            player.sendMessage("On cooldown for " + getCooldown(player) + " seconds!");
        }
    }

    @Override
    public void run() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (!cooldowns.containsKey(player.getName())) {
                setCooldown(player, 0);
            }
            if (getCooldown(player) > 0) {
                setCooldown(player, getCooldown(player) - 1);
            }
        });
    }
}