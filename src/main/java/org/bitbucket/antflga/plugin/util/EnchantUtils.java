package org.bitbucket.antflga.plugin.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public final class EnchantUtils {

    public static void setEnchantment(Player player, ArmorSlot armorSlot, Enchantment enchantment, int level, int inventorySlot) {
        switch (armorSlot) {
            case HELMET:
                player.getInventory().getHelmet().addUnsafeEnchantment(enchantment, level);
                break;
            case CHESTPLATE:
                player.getInventory().getChestplate().addUnsafeEnchantment(enchantment, level);
                break;
            case LEGGINGS:
                player.getInventory().getLeggings().addUnsafeEnchantment(enchantment, level);
                break;
            case BOOTS:
                player.getInventory().getBoots().addUnsafeEnchantment(enchantment, level);
                break;
            case INVENTORY:
                player.getInventory().getItem(inventorySlot).addUnsafeEnchantment(enchantment, level);
        }
    }

    public enum ArmorSlot {
        HELMET,
        CHESTPLATE,
        LEGGINGS,
        BOOTS,
        INVENTORY
    }
}
