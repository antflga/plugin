package org.bitbucket.antflga.plugin.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public final class InventoryUtils {

    public static void addToInventory(Player player, int slot, Material item, int stackSize, short modifier) {
        player.getInventory().setItem(slot, new ItemStack(item, stackSize, modifier));
    }

    public static void addToInventory(Player player, int slot, ItemStack item) {
        player.getInventory().setItem(slot, item);
    }
}
