package org.bitbucket.antflga.plugin.util;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")
public enum ArmorType {
    CHAINMAIL,
    GOLD,
    IRON,
    DIAMOND
}
