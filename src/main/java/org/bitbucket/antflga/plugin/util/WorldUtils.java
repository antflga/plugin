package org.bitbucket.antflga.plugin.util;

import org.bitbucket.antflga.plugin.main.Plugin;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alice
 * @since 9/23/2015
 */
@SuppressWarnings("unused")

public final class WorldUtils {

    /**
     * @param player
     * @param node
     * @param playerCoordinates
     */
    public static void writeLocation(Player player, String node, List<Integer> playerCoordinates) {
        YmlUtils.setNode("plugin.locations.", node, playerCoordinates);
        player.sendMessage("plugin.locations." + node + playerCoordinates);
        player.sendMessage(node + " created.");
    }

    /**
     * @param player
     * @param node
     */
    public static void removeLocation(Player player, String node) {
        YmlUtils.deleteNode("plugin.locations." + node);
        player.sendMessage("Node " + node + " now deded!");
    }

    /**
     * @param node
     * @return
     */
    public static String readLocation(String node) {
        return node + ": " + YmlUtils.getIntegerList("plugin.locations.", node);
    }

    public static void wipeLocations() {
        YmlUtils.deleteNode("plugin.locations");
    }

    /**
     * @param player
     * @param node
     * @param args2
     * @param args3
     */
    public static void writeArea(Player player, String node, String args2, String args3) {
        List<String> locations = new ArrayList<>();
        locations.add(args2);
        locations.add(args3);
        YmlUtils.setNode("plugin.areas.", node + ".locations", locations);
        YmlUtils.setNode("plugin.areas.", node + ".coordinates.setOne", YmlUtils.getIntegerList("plugin.locations", args2));
        YmlUtils.setNode("plugin.areas.", node + ".coordinates.setTwo", YmlUtils.getIntegerList("plugin.locations", args3));
        player.sendMessage(node + " created.");
    }

    /**
     * @param player
     * @param node
     */
    public static void removeArea(Player player, String node) {
        YmlUtils.deleteNode("plugin.areas." + node);
        Plugin.saveYml(Plugin.getLocationConfig(), Plugin.getLocationYml());
        player.sendMessage("Node " + node + " now deded!");
    }

    /**
     * @param node
     * @return
     */
    public static String readArea(String node) {
        return node + ": " + Plugin.getLocationConfig().getIntegerList("plugin.areas." + node);
    }

    public static void wipeAreas() {
        YmlUtils.deleteNode("plugin.areas");
    }

    public static boolean isAtLocation(Player player, String location) {
        List<Integer> locCoords = YmlUtils.getIntegerList("plugin.locations", location);
        Location loc = new Location(player.getWorld(), locCoords.get(0), locCoords.get(1), locCoords.get(2));
        return player.getLocation().equals(loc);
    }

    /**
     * @param player
     * @param location
     * @param material
     * @param hollow
     */
    public static void placeAtCoordinates(Player player, String location, Material material, boolean hollow) {
        String ymlPath = "plugin.areas." + location + ".coordinates.";
        int x1 = YmlUtils.getIntegerList(ymlPath, "setOne").get(0);
        int y1 = YmlUtils.getIntegerList(ymlPath, "setOne").get(1);
        int z1 = YmlUtils.getIntegerList(ymlPath, "setOne").get(2);
        int x2 = YmlUtils.getIntegerList(ymlPath, "setTwo").get(0);
        int y2 = YmlUtils.getIntegerList(ymlPath, "setTwo").get(1);
        int z2 = YmlUtils.getIntegerList(ymlPath, "setTwo").get(2);
        int buffer;

        if (x1 > x2) {
            buffer = x1;
            x1 = x2;
            x2 = buffer;
        }
        if (y1 > y2) {
            buffer = y1;
            y1 = y2;
            y2 = buffer;
        }
        if (z1 > z2) {
            buffer = z1;
            z1 = z2;
            z2 = buffer;
        }

        for (int x = x1; x - 1 < x2; x++) {
            for (int y = y1; y - 1 < y2; y++) {
                for (int z = z1; z - 1 < z2; z++) {
                    Location loc = new Location(player.getWorld(), x, y, z);
                    loc.getBlock().setType(material);
                }
            }
        }

        if (hollow) {
            for (int x = x1; x < x2; x++) {
                for (int y = y1; y < y2; y++) {
                    for (int z = z1; z < z2; z++) {
                        if (x != x1 && z != z1 && y != y1) {
                            Location loc = new Location(player.getWorld(), x, y, z);
                            loc.getBlock().setType(Material.AIR);
                        }
                    }
                }
            }
        }
    }
}