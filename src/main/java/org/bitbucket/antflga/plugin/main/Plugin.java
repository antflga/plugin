package org.bitbucket.antflga.plugin.main;

import org.bitbucket.antflga.plugin.commands.Area;
import org.bitbucket.antflga.plugin.commands.LocationCommand;
import org.bitbucket.antflga.plugin.pvp.HeroHandler;
import org.bitbucket.antflga.plugin.pvp.PVPListener;
import org.bitbucket.antflga.plugin.util.Cooldown;
import org.bitbucket.antflga.plugin.util.HeroAssigner;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author alice
 * @since 8/30/2015
 */
@SuppressWarnings("unused")
public class Plugin extends JavaPlugin {

    private static File pluginDataDir = new File("plugins/plugin/");
    static File location = new File(pluginDataDir.getPath().concat("/plugin.yml"));
    static FileConfiguration locationConfig = YamlConfiguration.loadConfiguration(location);
    private Logger log = Logger.getLogger("Minecraft");

    public static String getDirectory() {
        return pluginDataDir.getPath();
    }

    public static File getLocationYml() {
        return location;
    }

    public static FileConfiguration getLocationConfig() {
        return locationConfig;
    }

    public static void saveYml(FileConfiguration ymlConfig, File ymlFile) {
        try {
            locationConfig.save(ymlFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEnable() {
        getCommand("hero").setExecutor(new HeroHandler());
        getCommand("location").setExecutor(new LocationCommand());
        getCommand("area").setExecutor(new Area());

        new PVPListener(this);

        if (!pluginDataDir.exists()) {
            pluginDataDir.mkdir();
        }

        BukkitTask heroes = new HeroAssigner(this).runTaskTimer(this, 5, 5);
        BukkitTask cooldown = new Cooldown(this).runTaskTimer(this, 20, 20);
    }

    @Override
    public void onDisable() {

    }
}
